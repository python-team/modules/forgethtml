forgethtml (0.0.20031008-12) UNRELEASED; urgency=medium

  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <novy@ondrej.org>  Tue, 29 Mar 2016 21:37:22 +0200

forgethtml (0.0.20031008-11) unstable; urgency=low

  * Team upload.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  * Drop obsolete Conflicts/Replaces with python2.3-forgethtml and
    python2.4-forgethtml.

  [ Larissa Reis ]
  * Drops deprecated pysupport and uses dh-python instead (Closes: #786052)

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 24 Aug 2015 15:43:28 +0200

forgethtml (0.0.20031008-10) unstable; urgency=low

  [ Piotr Ożarowski ]
  * Added Vcs-Svn field

  [ Bernd Zeimetz ]
  * Added Vcs-Browser field
  * Fixed the following lintian errors (closes: #592387):
    - clean-should-be-satisfied-by-build-depends
      python | python-dev | python-all-dev
    - missing-build-dependency python-support (>= 0.3)

  [ Sandro Tosi ]
  * debian/watch
    - added missing file
  * debian/control
    - added op=log to Vcs-Browser
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Carlos Galisteo ]
  * debian/control
    - Added Homepage field.

  [ Morten Werner Forsbring ]
  * Changed my lastname in debian/control.
  * Bumped standards-version from 3.7.2 to 3.9.1 (no changes).
  * Bumped debhelper compat level from 4 to 5.
  * Fixed more lintian errors:
    - debhelper-but-no-misc-depends (added ${misc:Depends} to depends)
    - build-depends-on-python-dev-with-no-arch-any (changed from
      python-dev to python-all in Build-Depend)
  * Added patch that remove non-ASCII-characters in forgetHTML.py.

 -- Morten Werner Forsbring <werner@debian.org>  Sat, 09 Apr 2011 12:27:25 +0200

forgethtml (0.0.20031008-9) unstable; urgency=low

  * Only provides ${python:Provides} as we only want to provide packages
    for supported python versions. (Closes: #399932)
  * Bumped Standards-Version (no changes needed).

 -- Morten Werner Olsen <werner@debian.org>  Thu, 23 Nov 2006 00:23:28 +0100

forgethtml (0.0.20031008-8) unstable; urgency=low

  * Update for new Python policy. Closes: #373430

 -- Raphael Hertzog <hertzog@debian.org>  Sat, 17 Jun 2006 23:41:24 +0200

forgethtml (0.0.20031008-7) unstable; urgency=low

  * Switching to CDBS and starting to team maintain the package within the
    Debian Python Modules Team (changing Maintainer address and adding
    myself as uploader).
  * Moving debhelper and cdbs to Build-Depends.
  * Fixing up the use of python-support.
     - Removed python:Depends variable and adding dependency to python
       (>= 2.3) and python-support in debian/control.
     - Moving files from site-packages to python-support in debian/rules.
     - Adding python-support stuff in postinst and prerm.

 -- Morten Werner Olsen <werner@debian.org>  Mon,  1 May 2006 11:39:01 +0200

forgethtml (0.0.20031008-6) unstable; urgency=low

  * Now using the new version of dh_python (with debhelper 5.0.14).
     - This eliminates the need of having one version of the package per
       supported Python version, so removing the python2.3-forgethtml
       package and moving all the files into python-forgethtml.
     - Changed debhelper version in Build-Depends to (>= 5.0.13).
     - Added 'Provides/Conflicts/Replaces: python2.3-forgethtml,
       python2.4-forgethtml' to package python-forgethtml.
     - Changing from using python2.3 to python in debian/rules.
     - Changing from Build-Depends to Build-Depends-Indep in debian/control.
     - Now Arch: all (thanks to Martin Michlmayr). Closes: #355354
  * Bumped Standards-Version to 3.6.2 (no changes).
  * New maintainer address. :)

 -- Morten Werner Olsen <werner@debian.org>  Mon,  6 Feb 2006 22:26:08 +0100

forgethtml (0.0.20031008-5) unstable; urgency=low

  * The embarrasing "Fixed another typo in package description"-release.
    Thanks to H. Cauwelier. (Closes: #269904)

 -- Morten Werner Olsen <werner@skolelinux.no>  Tue,  5 Oct 2004 18:22:13 +0200

forgethtml (0.0.20031008-4) unstable; urgency=low

  * Fixed typo in package description. Thanks to Malcolm Parsons.
    (Closes: #266590)

 -- Morten Werner Olsen <werner@skolelinux.no>  Thu, 19 Aug 2004 01:10:04 +0200

forgethtml (0.0.20031008-3) unstable; urgency=low

  * Added Build-Depends on python. Thanks to Andreas Jochens.
    (Closes: #266524)

 -- Morten Werner Olsen <werner@skolelinux.no>  Wed, 18 Aug 2004 10:33:32 +0200

forgethtml (0.0.20031008-2) unstable; urgency=low

  * Made python-forgethtml "Arch: all". Thanks to Randall Donald for pointing
    this out.

 -- Morten Werner Olsen <werner@skolelinux.no>  Tue, 17 Aug 2004 14:34:23 +0200

forgethtml (0.0.20031008-1) unstable; urgency=low

  * Initial release. (Closes: #262936)

 -- Morten Werner Olsen <werner@skolelinux.no>  Mon,  2 Aug 2004 13:22:07 +0200
